using UnityEngine;
using System.Collections;

/*
 * This class handles input provided by the Zigfader
 * 
 */
public class EventController : MonoBehaviour {
	
	//GAMEOBJECTS
	//array containing all selectable events
	public GameObject[] items;

	
	//SHOW / HIDE DURING SESSION
	public GameObject[] ShowDuringSession;
	public GameObject[] HideDuringSession;
	
	//NUB / SELECTOR
	public Transform nub;
	private int pushCount = 0;
	
	public Transform marker_topRight;
	public Transform marker_topLeft;
	public Transform marker_bottomLeft;
	
	//Fader
	private float controlledFaderValue;
	private float faderWorkingRange = 0.15f;
	private float faderIncreaseSpeed = 0;
	
	//GUI
	private bool showLoadingTitle = false;
	
	void Fader_HoverStart(ZigFader fader) {
		
	}
	
	void Fader_HoverStop(ZigFader fader) {
	}
	
	void Fader_ValueChange(ZigFader fader) {
		
		if(fader.value < 0 + faderWorkingRange){
			if(controlledFaderValue >= -1){
			controlledFaderValue-= 0.003f + faderIncreaseSpeed;	
			}
		}
		
		if(fader.value > 1 - faderWorkingRange){
			if(controlledFaderValue <=1){
			controlledFaderValue+= 0.003f + faderIncreaseSpeed;	
			}
		}
		
		if(fader.value> 0 + faderWorkingRange && fader.value < 1 - faderWorkingRange){
			faderIncreaseSpeed = 0;
		}else {
			faderIncreaseSpeed += 0.000059f;
			
		}
		fader.correctedValue = controlledFaderValue;
		if(controlledFaderValue >= 0){
		nub.localPosition = Vector3.Lerp (marker_topLeft.position, marker_topRight.position, controlledFaderValue);
		}
		if(controlledFaderValue < 0){
		nub.localPosition = Vector3.Lerp (marker_topLeft.position, marker_bottomLeft.position, controlledFaderValue * -1);
		}
		
	}
	
	//pushDetecorInput
	
	void PushDetector_Push() {
	
		Debug.Log ("Loading GameScene");
		PlayerPrefs.SetInt("backNavigation",1);
		showLoadingTitle = true;
		Application.LoadLevel("gameDemo");
	}
	
	void PushDetector_Hold() {
	}
	
	void PushDetector_Click() {
	}
	
	void PushDetector_Release() {
	}
	
	void Session_Start() {
		   foreach (GameObject go in ShowDuringSession)
        {
            go.SetActiveRecursively(true);
        }
        foreach (GameObject go in HideDuringSession)
        {
            go.SetActiveRecursively(false);
        }
		Debug.Log ("KinectController started session");
	}
	
	void Session_End() {
		   foreach (GameObject go in ShowDuringSession)
        {
            go.SetActiveRecursively(false);
        }
        foreach (GameObject go in HideDuringSession)
        {
            go.SetActiveRecursively(true);
        }
		Debug.Log ("KinectController ended session");
	}
	
	// UNITY 3D Lifecycle
	
	// Initalize
	void Start () {	
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	// GUI
	void OnGUI () {
		if(showLoadingTitle){
		GUI.Box (new Rect (500,150,100,30), "Loading...");
		}
		
	}
}
