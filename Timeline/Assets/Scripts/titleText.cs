using UnityEngine;
using System.Collections;

public class titleText : MonoBehaviour {
	
	/*
	 * This Script is supposed to add dynamically the event's title
	 * The value of the String "gameTitle" isn't set yet.
	 * 
	 */
	
	string gameTitel = "empty";
	
	// Use this for initialization
	void Start () {
	gameObject.GetComponent<TextMesh>().text = gameTitel;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
