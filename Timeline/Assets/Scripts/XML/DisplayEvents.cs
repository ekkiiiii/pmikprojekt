using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// THIS CLASS WILL DISPLAY THE TEXT ON THE GAME SCENE
// EDITOR: Peter
public class DisplayEvents : MonoBehaviour {
	
	private List<Event> eventList = new List<Event>();
	
	// Use this for initialization
	void Start () {
	XMLParser parser = new XMLParser();
		eventList = parser.ReadXML();
		for(int i =0; i< eventList.Count;i++){
			gameObject.GetComponent<TextMesh>().text = eventList[i].GetTitle();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
