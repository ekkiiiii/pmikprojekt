using UnityEngine;
using System.Collections;
using System;


// THIS CLASS REPRESENTS AN EVENT READ FROM "timeline.xml"
// EDITOR: Peter
public class Event {
	// PUBLIC
	
	// PRIVATE
	private String title;
	private String description;
	
	
	
	public Event(String title, String description){
		this.description = description;
		this.title = title;
		
	}
	
	
	// GETTER & SETTER
	
	public String GetDescription(){
		return description;	
	}
	
	public String GetTitle(){
		return title;	
	}

}
