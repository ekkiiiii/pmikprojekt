using System.Collections;
using System.Xml;
using System.Text;
using System;
using System.Collections.Generic;
using UnityEngine;

// THIS CLASS IS FOR READING DATA FROM THE "Timline.xml" FILE
// EDITOR: Peter



public class XMLParser {
	// PUBLIC
	
	// PRIVATE
	private String xmlFile = @"Assets/XML/timeline.xml";
	private String debugTag = @"XMLParser: ";
	
	public List<Event> ReadXML(){
		Debug.Log(debugTag + "reading " + xmlFile);
		List<Event> eventList = new List<Event>();
		
		//set up xmlReader
		XmlReaderSettings settings = new XmlReaderSettings();
		settings.ConformanceLevel = ConformanceLevel.Fragment;
		settings.IgnoreWhitespace = true;
		settings.IgnoreComments = true;
		XmlReader reader = XmlReader.Create(xmlFile,settings);
		
		// read
		reader.ReadToFollowing("title");
		string title = reader.ReadElementContentAsString();
		
		reader.ReadToFollowing("description");
		string description = reader.ReadElementContentAsString();
		
		// add Event to List
		eventList.Add(new Event(title,description));
		
		// return created list
		return eventList;
	}
	
	
	
	

}