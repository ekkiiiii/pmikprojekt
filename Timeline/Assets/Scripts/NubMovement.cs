using UnityEngine;
using System.Collections;
using System;

public class NubMovement : MonoBehaviour {
	
	// PUBLIC
	public int nubSpeed;
	
	public Transform MoveTextMesh;
	
	
	// PRIVATE
	private String debugTag = "Nub: ";
	

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	MoveTextMesh.transform.position = new Vector3(gameObject.transform.position.x,MoveTextMesh.position.y,MoveTextMesh.position.z);
		
	}
	
	

}
