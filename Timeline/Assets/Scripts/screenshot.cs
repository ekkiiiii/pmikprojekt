using UnityEngine;
using System.Collections;

public class screenshot : MonoBehaviour {

	/*
	 * This script pastes the textTextures to the cube "mainTextBackground"
	 * The dimensions of the texture scale the cube to its necessary size.
	 * 
	 */ 
	
	// anlegen der Texturen im Inspector
	public Texture2D[] textures;
	public int eventNumber = 0;
	public int inspectorShowEventNumber = -1;
	
		
	
	void Start () {
	
	// hier werden die Texturen auf das Objekt geklatsch
	renderer.material.mainTexture = textures[PlayerPrefs.GetInt("eventNumber")];

	}
	
}
