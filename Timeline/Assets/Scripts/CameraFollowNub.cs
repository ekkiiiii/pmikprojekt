using UnityEngine;
using System.Collections;

public class CameraFollowNub : MonoBehaviour {
	
	public Transform controllerNub;
	
	public int X_Offset;
	public int Y_Offset;
	
	public int initial_X_Offset;
	public int initial_Y_Offset;
	
	private float calculated_X_Offset;
	private float calculated_Y_Offset;

	// Use this for initialization
	void Start () {
	transform.localPosition = new Vector3(initial_X_Offset + controllerNub.position.x + calculated_X_Offset,initial_Y_Offset + controllerNub.position.y + calculated_Y_Offset,gameObject.transform.position.z);
	}
	
	
	void Fader_ValueChange(ZigFader fader) {
		
	if(fader.correctedValue < 0){
	calculated_Y_Offset = Mathf.Lerp(0,Y_Offset,fader.correctedValue* -1);
	
	}
		
	if(fader.correctedValue >= 0){
	calculated_X_Offset = Mathf.Lerp(0,X_Offset,fader.correctedValue);
	}
		
	}
	
	// Update is called once per frame
	void Update () {
	transform.localPosition = new Vector3(initial_X_Offset + controllerNub.position.x + calculated_X_Offset,initial_Y_Offset + controllerNub.position.y + calculated_Y_Offset,gameObject.transform.position.z);
	}
}
