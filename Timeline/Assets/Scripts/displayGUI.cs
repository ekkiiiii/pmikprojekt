using UnityEngine;
using System.Collections;

// THIS CLASS IS FOR GUI HANDLING
// EDITOR: PETER
public class DisplayGUI : MonoBehaviour {
	// if true GUI is shown
	public bool showGUI;	
	public string guiOutput1 = "Wave to start!";
	public string guiOutput2 = "Move Your Hand left or right to select an event";
	
	void Start () {
	
	}
	
	void Update () {
	}
	
	void OnGUI () {
		if(showGUI){
		// GUI declaration
		GUI.Label(new Rect(10,10,500,50), guiOutput1);
		GUI.Label(new Rect(10,30,500,50), guiOutput2);
		}
	}
	
	
	// Display passed text on gui(top left) on upper label
	public void SetGuiOutput1(string text){
		this.guiOutput1 = text;
	}
	
	// Display passed text on gui(top left) on lower label
	public void SetGuiOutput2(string text){
		this.guiOutput2 = text;
	}
	
}
