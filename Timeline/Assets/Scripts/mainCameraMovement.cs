using UnityEngine;
using System.Collections;
using System;


// THIS CLASS SHOULD BE RESPONSIBLE FOR CAMERA MOVEMENTS
// EDITOR: Peter
public class mainCameraMovement : MonoBehaviour {
	
	// PUBLIC
	public int cameraSpeed;
	
	// PRIVATE
	private bool initMovementFinished;
	private String debugTag = "Main Camera: ";
	// These Vector3 positions can also be set by gameObjects and their positions
	private Vector3 startPosition = new Vector3(-2.33f,6,-30);
	private Vector3 endPosition = new Vector3(-1.04f,2f,-11f);
	private float journeyLength;
	private float startTime;
	
	// Initialize on start
	void Start () {
	initMovementFinished = false;
	startTime = Time.time;
	transform.position = new Vector3(-2.33f,6,-30);
	journeyLength = Vector3.Distance(startPosition, endPosition);
	Debug.Log(debugTag + "started");
	}
	
	
	void Update () {
		// Initial movement
		if(PlayerPrefs.GetInt("backNavigation") == 0){	
		if(!initMovementFinished){
		float distCovered = (Time.time - startTime) * cameraSpeed;
        float fracJourney = distCovered / journeyLength;
			if(fracJourney >= 0.99f){
			initMovementFinished = true;
			MainCameraSmoothFollow mcsf = gameObject.GetComponent<MainCameraSmoothFollow>();
			mcsf.SetSmoothFollowActive(true);
			}
		transform.position = Vector3.Lerp(startPosition, endPosition, fracJourney);
		}
		}	
	}
}
