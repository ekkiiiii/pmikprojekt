using UnityEngine;
using System.Collections;

public class mainText : MonoBehaviour {
	
	/*
	 * This script pastes the textTextures to the cube "mainTextBackground"
	 * The dimensions of the texture scale the cube to its necessary size.
	 * 
	 */ 
	
	// anlegen der Texturen im Inspector
	public Texture2D[] textures;
	//public int eventNumber = 0;
	
		void calcSize(int eventNumber){
			float ratio = 0;
			
			//Zunächst wird das Seitenverhältnis der Textur berechnet
		if(textures[eventNumber].width > textures[eventNumber].height){
			
			ratio = (float)textures[eventNumber].width / (float)textures[eventNumber].height;			
		} 
		
		else if (textures[eventNumber].width < textures[eventNumber].height){
			ratio = (float)textures[eventNumber].height / (float)textures[eventNumber].width;
		}
		
		
		
		// Hier wird die Skalierung der Y-Achse vorgenommen
		float x = transform.localScale.x;
		float yNew = x * ratio;
		GameObject anchor = GameObject.Find("Anchor");
		anchor.transform.localScale += new Vector3(0,yNew/60,0);
	}
	
	
	
	
	void Start () {
	
	calcSize(PlayerPrefs.GetInt("eventNumber"));

	// hier werden die Texturen auf das Objekt geklatsch
	renderer.material.mainTexture = textures[PlayerPrefs.GetInt("eventNumber")];
	Debug.Log(PlayerPrefs.GetInt ("eventNumber"));
	}
	

}