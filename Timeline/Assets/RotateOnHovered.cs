using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
    void Update() {
		// Slowly rotate the object around its Y axis at 20 degree/second.
        transform.Rotate(Vector3.up * Time.deltaTime * 20);	
    }
}
