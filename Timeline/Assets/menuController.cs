using UnityEngine;
using System.Collections;

/*
 * This class handles input provided by the Zigfader
 * 
 */
public class menuController : MonoBehaviour {

	//Events to calculate positions
	public Transform firstEvent;
	public Transform lastEvent;
	
	//GAMEOBJECTS
	//array containing all selectable events
	public GameObject[] items;
	public GameObject[] covers;
		
	public GameObject pushText;
	public GameObject moveText;
	
	// COLORS
	public Color hoverColor = Color.green;
    public Color clickColor = Color.blue;
    public Color pushColor = Color.gray;
    public Color holdColor = Color.yellow;
    public Color heldReleaseColor = Color.red;
	private Color origColor;
	
	// TIMELINE
	public GameObject Timeline;
	
	//SHOW / HIDE DURING SESSION
	public GameObject[] ShowDuringSession;
	public GameObject[] HideDuringSession;
	
	//NUB / SELECTOR
	public Transform nub;
	public int currentItem = 11;
	bool clicked = false;
	public int itemCount = 20;
	private int pushCount = 0;
	private int lastSelectedItem;
	
	//Fader
	private float controlledFaderValue = 0.5f;
	private float faderWorkingRange = 0.15f;
	private float faderIncreaseSpeed = 0;
	
	//GUI
	private bool showLoadingTitle = false;
	
	void Fader_HoverStart(ZigFader fader) {	
		lastSelectedItem = fader.lastItem;
		currentItem = fader.hoverItem;
		if(lastSelectedItem != currentItem){
		moveText.SetActive(true);
		pushText.SetActive(false);
		}
		
		// Set Cover transparency
		if(currentItem > 1 ) Fade(0.05f,currentItem-2);
		if(currentItem > 0 ) Fade(0.3f,currentItem-1);
		Fade(0.8f,currentItem);
		if(currentItem < 19) Fade(0.3f,currentItem+1);
		if(currentItem < 18) Fade(0.05f,currentItem+2);
		
		origColor = items[currentItem].renderer.material.color;
        items[currentItem].renderer.material.color = hoverColor;
		Debug.Log ("HoverStart: " + currentItem.ToString());
	}
	
	//return a new color based on gameobject and passed alpha
	private void Fade(float toalpha,int covernumber){

		covers[covernumber].renderer.material.color = new Color(covers[covernumber].renderer.material.color.r,
																covers[covernumber].renderer.material.color.g,
																covers[covernumber].renderer.material.color.b,
																toalpha);	
	}
	
	void Fader_HoverStop(ZigFader fader) {
		items[currentItem].renderer.material.color = origColor; 
		Debug.Log ("HoverStop: " + currentItem.ToString());
	}
	
	void Fader_ValueChange(ZigFader fader) {
		
		// NUB MOVEMENT
		if(fader.value < 0 + faderWorkingRange){
			if(controlledFaderValue >= 0){
			controlledFaderValue-= 0.003f + faderIncreaseSpeed;	
			}
		}
		
		if(fader.value > 1 - faderWorkingRange){
			if(controlledFaderValue <= 1){
			controlledFaderValue+= 0.003f + faderIncreaseSpeed;	
			}
		}
		
		if(fader.value> 0 + faderWorkingRange && fader.value < 1 - faderWorkingRange){
			faderIncreaseSpeed = 0;
		}else {
			faderIncreaseSpeed += 0.000035f;
			
		}
		// SET CORRECTED FADER VALUE IN ZIGFADER
		fader.correctedValue = controlledFaderValue;
		nub.localPosition = Vector3.Lerp (firstEvent.position, lastEvent.position, controlledFaderValue);
	}
	
	//pushDetecorInput
	
	void PushDetector_Push() {
		// SET PUSH COUNT
		if(lastSelectedItem != currentItem) {
			pushCount = 1; 
		} else {
			pushCount+=1;
		}
		
		// FIRST PUSH
		if(pushCount == 1){
		moveText.SetActive(false);
		pushText.SetActive(true);
		Debug.Log ("First Push " + currentItem);
		items[currentItem].renderer.material.color = pushColor;
		Fade(1.0f,currentItem);
		}
		
		//SECOND PUSH
		if(pushCount == 2){
		items[currentItem].renderer.material.color = pushColor;
		clicked = false;
		Debug.Log ("Loading Eventscene: " + currentItem);
		PlayerPrefs.SetInt("eventNumber",currentItem);
		showLoadingTitle = true;
		Application.LoadLevel("eventScene");
		}
		
		lastSelectedItem = currentItem;
	}
	
	void PushDetector_Hold() {
		clicked = false;
        items[currentItem].renderer.material.color = holdColor;
	}
	
	void PushDetector_Click() {
		clicked = true;
        items[currentItem].renderer.material.color = clickColor;
	}
	
	void PushDetector_Release() {
		        if (!clicked)
        {
            items[currentItem].renderer.material.color = heldReleaseColor;
        }
        
	}
	
	void Session_Start() {
		   foreach (GameObject go in ShowDuringSession)
        {
            go.SetActiveRecursively(true);
        }
        foreach (GameObject go in HideDuringSession)
        {
            go.SetActiveRecursively(false);
        }
		Debug.Log ("KinectController started session");
	}
	
	void Session_End() {
		   foreach (GameObject go in ShowDuringSession)
        {
            go.SetActiveRecursively(false);
        }
        foreach (GameObject go in HideDuringSession)
        {
            go.SetActiveRecursively(true);
        }
		Debug.Log ("KinectController ended session");
	}
	
	// UNITY 3D Lifecycle
	
	// Initalize
	void Start () {
	
	for(int i=0; i<=19;i++){
		covers[i].renderer.material.color = new Color(1,1,1,0.1f);
		}
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	// GUI
	void OnGUI () {
		if(showLoadingTitle){
		GUI.Box (new Rect (500,50,100,50), "Loading...");
		}
		
	}
}
